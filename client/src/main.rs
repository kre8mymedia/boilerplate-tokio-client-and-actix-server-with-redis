mod post;
mod get;

extern crate job_scheduler;
use job_scheduler::{JobScheduler, Job};
use std::time::Duration;

fn main() {

    let mut sched = JobScheduler::new();

    sched.add(Job::new("* * * * * *".parse().unwrap(), || {
        // 10x in 1 second = 0.1 seconds
        for x in 0..10 {
            // run the get run module
            println!("0.{}", x);
            post::run();
            get::run();
        }
    }));
    
    loop {
        sched.tick();

        std::thread::sleep(Duration::from_millis(500));
    }
}