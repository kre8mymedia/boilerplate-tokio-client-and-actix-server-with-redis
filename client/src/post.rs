use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
struct Person {
    user_id: String
}

#[derive(Deserialize, Debug)]
struct User {
    user_id: Option<String>,
    counter: u32,
}

#[derive(Debug, Serialize, Deserialize)]
struct PersonResponse {
    user_id: String,
    counter: u32
}

pub fn run() {
    post_request();
    add_request();
}


#[tokio::main]
async fn post_request() -> Result<(), Box<dyn std::error::Error>> {
    let p = Person { 
        user_id: "dfasfadfsa".to_string()
    };

    let res = reqwest::Client::new()
        .post("http://localhost:8080/login")
        .json(&p)
        .send()
        .await?;

    let js = res
        .json::<PersonResponse>()
        .await?;

    println!("{:#?}", js);

    Ok(())
}

#[tokio::main]
async fn add_request() -> Result<(), Box<dyn std::error::Error>> {

    let res = reqwest::Client::new()
        .post("http://localhost:8080/do_something")
        .send()
        .await?;

    let js = res
        .json::<User>()
        .await?;

    println!("{:#?}", js);

    Ok(())
}