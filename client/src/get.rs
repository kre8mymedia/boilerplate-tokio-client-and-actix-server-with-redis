use serde::Deserialize;

#[derive(Deserialize, Debug)]
struct User {
    user_id: Option<String>,
    counter: u32,
}

pub fn run() {
    get_request();
}

#[tokio::main]
async fn get_request() -> Result<(), Box<dyn std::error::Error>> {
    // Build the client using the builder pattern
    let client = reqwest::Client::builder()
        .build()?;

    // Perform the actual execution of the network request
    let res = client
        .get("http://localhost:8080")
        .send()
        .await?;

    // Parse the response body as Json in this case
    let user = res
        .json::<User>()
        .await?;

    println!("{:#?}", user);
    Ok(())
}